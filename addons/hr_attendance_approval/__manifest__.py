# Copyright 2023 Bahtera - Daniel D Fortuna
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Attendance Approval",
    "summary": "Manual Attendance from `My Attendance` need Approval from manager",
    "version": "16.0.1.0.0",
    "category": "Human Resources",
    "author": "Bahtera",
    "license": "LGPL-3",
    "installable": True,
    "depends": ["hr_attendance"],
    "data": [
        "views/hr_attendance_views.xml"
    ],
    "sequence": 1,
}
