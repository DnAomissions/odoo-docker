# -*- coding: utf-8 -*-

from odoo import models, fields, exceptions, _

class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    state = fields.Selection([
        ('draft', 'Draft'),
        ('approved', 'Approved'),
    ], default='draft', readonly=True)
    attendance_source = fields.Selection([
        ('manual', 'Manual'),
        ('kiosk', 'Kiosk')
    ], default='manual', readonly=True)

    def action_approve(self):
        if not self.env.user.has_group('hr_attendance.group_hr_attendance_user'):
            raise exceptions.UserError(_('Permission denied. Only Officer : Manage all attendances.'))
        self.write({
            'state': 'approved'
        })
        return True