# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, exceptions, _

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    def _attendance_action(self, next_action):
        res = super(HrEmployee, self)._attendance_action(next_action)
        if next_action == 'hr_attendance.hr_attendance_action_kiosk_mode' and self.attendance_state == 'checked_in':
            attendance = self.env['hr.attendance'].search([('employee_id', '=', self.id), ('check_out', '=', False)], limit=1)
            if attendance:
                attendance.write({
                    'state': 'approved',
                    'attendance_source': 'kiosk',
                })
            else:
                raise exceptions.UserError(_('Cannot perform check out on %(empl_name)s, could not find corresponding check in. '
                    'Your attendances have probably been modified manually by human resources.') % {'empl_name': self.sudo().name, })
        return res